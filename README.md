TempoValence
============

*If viewing anywhere other than `gitlab.com/matt34Tea`, please note repo has moved, with origin now at my personal [GitLab account](https://gitlab.com/matt34Tea/tempo-valence) to make use of CI pipeline*


Search Spotify's api for music based on its Tempo (beats per minute) and Valence (positivity or happiness)

------

## To run app

1. Clone repo to your local machine

2. Navigate to root folder

3. Run the following from the command line...

    ```bash
    ./runlocal.sh
    ```
    
    Note - `runlocal.sh` is not included in the public repo as it contains private environment variables
    - To set locally and run app, use the following...
    
    ```bash
    export CLIENT_KEY=private-key
    
    ./gradlew run
    ```

    To run tests from within IntelliJ the `System.getenv("VARIABLE_NAME")` probably won't work.
    
    So, add CLIENT_KEY env var to the project run configuration -> Run > Edit Configurations... > Environment variables: (Browse... button)
    
    Visit the following...
    - `http://localhost:9000` -> route of app
    - `http://localhost:9000/tracks` -> selection of tracks without valence query param
    - `http://localhost:9000/tracks?valence=0.7` -> selection of tracks above valence query param
    
------

## Steps to run on k8s cluster

As this cluster and application will not be continually running here are the steps needed before this will be available on the internet

------

### To setup

```bash
gcloud container clusters create tempo-valence-cluster

# Run ci pipeline, or...
kubectl create deployment tempo-valence --image=gcr.io/inductive-seer-256007/tempo-valence:latest

kubectl scale deployment tempo-valence --replicas=2

kubectl autoscale deployment tempo-valence --cpu-percent=80 --min=1 --max=5

kubectl expose deployment tempo-valence --name=tempo-valence-service --type=LoadBalancer --port 9000 --target-port 9000
```

------


### To teardown

```bash
kubectl delete service tempo-valence-service

gcloud container clusters delete tempo-valence-cluster
```


------

## Technologies

- Kotlin
- http4k (http framework)
- result4k (for exception handling) - planned
- Gitlab CI
- GCP storage and container registry (with jib for Docker)
- Kubernetes

------

## Structure

Structure of code...

- Methods that handle initial http request (from UI, eg)
- Methods that deal with the external api/client http requests
- Use repository pattern

------

## Spotify api

Link to spotify [api console](https://developer.spotify.com/documentation/web-api/)
